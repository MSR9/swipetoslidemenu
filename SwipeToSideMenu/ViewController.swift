//
//  ViewController.swift
//  SwipeToSideMenu
//
//  Created by EPITADMBP04 on 3/19/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var home: UIButton! {
        didSet{
            self.home.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var aboutUs: UIButton! {
        didSet{
            self.aboutUs.layer.cornerRadius = 10
        }
    }
    
    @IBOutlet weak var viewConstriant: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var sideView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.layer.cornerRadius = 15
        sideView.layer.shadowColor = UIColor.orange.cgColor
        sideView.layer.shadowOpacity = 1
        sideView.layer.shadowOffset = CGSize(width: 6, height: 0)
        viewConstriant.constant = -175
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func panPerformed(_ sender: UIPanGestureRecognizer) {
        
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: self.view).x
            if translation  > 0 {  // swipe right
                if viewConstriant.constant < 20 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstriant.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
            } else {    // swipe left
                if viewConstriant.constant > -175 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstriant.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
            }
        } else if sender.state == .ended {
            if viewConstriant.constant < -100 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstriant.constant = -175
                    self.view.layoutIfNeeded()
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstriant.constant = 0
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}
